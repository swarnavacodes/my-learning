// const squere = function (x) {
//     return x * x
// }


//const squere = (x) => { return x * x}

const squere = (x) => x * x

console.log(squere(2))

const myEvent = {
    name: 'Birthday Party',
    guestList: ['Swarnava','Chumki','Sonakshi'],
    printGuestList() {
        console.log('Guest list for '+this.name)

        this.guestList.forEach((guest)=> {
            console.log(guest + ' is attending '+ this.name)
        })
    }
}
// this is how method in object is being used
myEvent.printGuestList()