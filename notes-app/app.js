const validator= require('validator')
const note = require('./notes.js')
const chalk = require('chalk');
const yargs= require('yargs')
const log = console.log;
const notes = require('./notes.js');
const { argv } = require('yargs');


// log(`
// CPU: ${chalk.blue('9%')}
// RAM: ${chalk.green('40%')}
// DISK: ${chalk.yellow('70%')}
// `);
// console.log(chalk.blue('Hello world!'));
// console.log(note())
// console.log(validator.isEmail('abc@gmail.com'))
// console.log(validator.isURL('https://google.com'))

// const command =process.argv[2]

// if(command==='add')
// {
//     console.log('Adding 2 numbers')
// }



yargs.command({
        command: 'add',
        describe: 'Add a new note!!',
        builder: {
            title: {
                describe: 'Add a new note',
                demandOption: true,
                type: 'string'
            },
            body: {
                describe: 'Body goes here',
                demandOption: true,
                type: 'string'
            }

        },
        handler: function (argv){
            note.addNote(argv.title,argv.body)
        }
})

yargs.command({
    command: 'remove',
    describe: 'Remove a note!!',
    builder: {
        title: {
            describe: 'Provide a title to remove',
            demandOption: true,
            type: 'string'
        }
    },
    handler:  ()=>{
        note.removeNote(argv.title)
    }
})

yargs.command({
    command: 'read',
    describe: 'Read a note!!',
    builder: {
        title: {
            describe: 'Provide a title to read',
            demandOption: true,
            type: 'string'
        }
    },
    handler:  ()=>{
        note.readNote(argv.title)
    }
})

yargs.command({
command: 'list',
describe: 'List all notes!!',
handler:  ()=>{
    note.listNotes()
}
})

yargs.parse()
//console.log(yargs.argv)
//else console.log('Doing nothing')

// const add=require('./utils.js')
// const sum= add(4, -2)
// console.log(sum)