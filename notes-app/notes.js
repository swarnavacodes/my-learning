const fs = require('fs')
const chalk = require('chalk');

const getNotes = function() {
    return 'Your notes...'}

// ADD Note
const addNote = function(title,body) {
    const notes =loadNotes()
    //const duplicateNotes = notes.filter( (note) => note.title === title)

    const duplicateNote = notes.find((note)=>note.title===title)
           
    if(!duplicateNote)
    {
        notes.push(
            {
                title: title,
                body: body
            }
        )
        saveNotes(notes)
        console.log(chalk.bgGreen('New note added'))
    }
    else
    {
        console.log(chalk.bgRed('Note already taken'))
    }
}
// Remove Note
const removeNote = (title)=> {
    const notes =loadNotes()
    const notesToKeep = notes.filter( (note) => note.title !== title
        
    )
    if(notesToKeep.length===notes.length)
    {
        console.log(chalk.bgRed('No note found to be removed'));
    }
    else
    {
        saveNotes(notesToKeep)
        console.log(chalk.bgGreen('Note removed'));
    }
}

// List all notes
const listNotes = () => {
    const notes =loadNotes()
    notes.forEach(element => {
        console.log(chalk.green(element.title))
    });
}

//Read note

const readNote= (title)=> {
    const notes =loadNotes()
    const foundNote = notes.find((note)=>note.title===title)

    if(foundNote)
    {
        console.log(chalk.green(foundNote.title))
        console.log(foundNote.body)
    }
    else
    console.log(chalk.red('No such note found'))

}

const saveNotes = (notes)=> {
    const dataJSON = JSON.stringify(notes)

    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes =  ()=> {

    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString
        return JSON.parse(dataBuffer)
    }
    catch (e)
    {
        return []
    }
}

//how to export multiple things from a js files
module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote:readNote
}